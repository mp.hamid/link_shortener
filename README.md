# Nginx Configuration

``````
server {
listen 80;

        server_name linkshortener.local;

        root /project-path/public/;
        index index.php;

        access_log /var/log/nginx/access-api.log;
        error_log /var/log/nginx/error-api.log;

        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }

        location ~ \.php$ {
             fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
             fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
             include fastcgi_params;
        }
}
``````

# Appache Configuration

``````
<VirtualHost *:80>
	ServerName linkshortener.local
	ServerAlias *.linkshortener.local
	DocumentRoot /project-path/public
	<Directory  "/project-path/public">
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Require all granted
	</Directory>
</VirtualHost>
``````

# Prepare Dependency Packages

``````
composer install
``````

# Generate JWT key
please run this line and enter passphrase charachter and put this string into PASSPHRASE key in .env file 
``````
ssh-keygen -t rsa -m pem -f ./storage/key-with-passphrase.pem
``````

#Import sql file
please import linkshortener.sql file to your database 

The default user has been added to the database, which you can enter in the login endpoint with the following data (username: test , password: test)

#Postman collection

The Postman file is exist at the root address of the project to access the api endpoints