<?php

use Pecee\SimpleRouter\SimpleRouter;

require __DIR__ . '/../vendor/autoload.php';

function exception_handler($exception)
{
    echo "Uncaught exception: ", $exception->getMessage(), "\n";
}

set_exception_handler('exception_handler');

$rootDirectory = __DIR__ . '/../';
$dotenv = Dotenv\Dotenv::createImmutable($rootDirectory);
$dotenv->load();
require_once $rootDirectory . 'routes.php';
SimpleRouter::setDefaultNamespace('\app\Controllers');
SimpleRouter::start();

