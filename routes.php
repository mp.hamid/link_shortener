<?php

use App\Controllers\AuthController;
use App\Controllers\LinkController;
use App\Controllers\RedirectController;
use App\Middleware\Authorize;
use Pecee\SimpleRouter\SimpleRouter;
SimpleRouter::get('/r/{uniqueHash}', [RedirectController::class, 'redirect'],['as'=>'redirect']);

SimpleRouter::group(['prefix' => '/api/v1'], function () {
    SimpleRouter::post('/login', [AuthController::class, 'login']);
    SimpleRouter::group(['middleware' => Authorize::class], function () {
        SimpleRouter::post('/link', [LinkController::class, 'add']);
        SimpleRouter::get('/link/{id}', [LinkController::class, 'get']);
        SimpleRouter::delete('/link/{id}', [LinkController::class, 'delete']);
        SimpleRouter::post('/link/{id}', [LinkController::class, 'update']);
    });
});