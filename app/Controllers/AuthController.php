<?php

namespace App\Controllers;

use App\Base\Facades\Database;
use Firebase\JWT\JWT;

class AuthController extends Controller
{
    public function __construct()
    {
    }

    public function login()
    {
        $values = input()->all();
        $username = $values['username'];
        $password = $values['password'];
        // Your passphrase
        $passphrase = $_ENV['PASSPHRASE'];
        $privateKeyFile = storage_path('key-with-passphrase.pem');
        $privateKey = openssl_pkey_get_private(
            file_get_contents($privateKeyFile),
            $passphrase
        );
        $result = Database::select("SELECT id,password FROM users WHERE username='" . $username . "';");
        if (is_null($result)) {
            return $this->responseJson(['status' => 'unauthorize'], '401');
        }
        if (!password_verify($password, $result['password'])) {
            return $this->responseJson(['status' => 'unauthorize'], '401');
        }
        $payload = [
            'id' => $result['id'],
            'username' => $username,
            'expire_at' => time() + $_ENV['TOKEN_EXPIRE_SECOND']
        ];

        $jwt = JWT::encode($payload, $privateKey, 'RS256');

        return $this->responseJson(['token' => $jwt]);
    }
}