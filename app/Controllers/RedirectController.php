<?php

namespace App\Controllers;

use App\Base\Facades\Cache;
use App\Base\Facades\Database;

class RedirectController extends Controller
{
    public function redirect($uniqueHash)
    {
        $uniqueHash = Database::escapeString($uniqueHash);
        if ($cacheLink = Cache::get($uniqueHash)) {
            response()->redirect($cacheLink);
        }
        $checkRow = Database::select("SELECT link FROM links WHERE short_hash='" . $uniqueHash . "';");
        if (is_null($checkRow)) {
            return 'link not exist';
        }
        Cache::set($uniqueHash, $checkRow['link']);
        response()->redirect($checkRow['link']);
    }

}