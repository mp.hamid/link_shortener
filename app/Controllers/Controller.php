<?php

namespace App\Controllers;

class Controller
{
    public function responseJson($data, $status = 200)
    {
        header('Content-Type: application/json; charset=utf-8');
        http_response_code($status);
        return json_encode($data);
    }

}