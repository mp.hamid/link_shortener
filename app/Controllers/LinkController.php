<?php

namespace App\Controllers;

use App\Base\Facades\Cache;
use App\Base\Facades\Database;

class LinkController extends Controller
{
    /**
     * @param $id
     * @return false|string
     */
    public function get($id): bool|string
    {
        $user = request()->user;
        $checkRow = Database::select("SELECT id,short_hash FROM links WHERE user_id='{$user->id}' AND id={$id};");
        if (is_null($checkRow)) {
            return $this->responseJson(['error' => 'row not found'], 404);
        }
        return $this->responseJson([
            'link' => url('redirect', ['uniqueHash' => $checkRow['short_hash']]),
            'id' => $checkRow['id']
        ]);
    }

    /**
     * @return false|string
     */
    public function add(): bool|string
    {
        $user = request()->user;
        $requestData = input()->all();
        if (!filter_var($requestData['link'], FILTER_VALIDATE_URL)) {
            return $this->responseJson(['error' => 'link is not a valid URL'], 421);
        }
        $checkRow = Database::select("SELECT short_hash,id FROM links WHERE link='" . $requestData['link'] . "';");
        if (!is_null($checkRow)) {
            return $this->responseJson([
                'link' => url('redirect', ['uniqueHash' => $checkRow['short_hash']]),
                'id' => $checkRow['id']
            ]);
        }
        try {
            Database::beginTransaction();
            $lastId = Database::insertWithId("INSERT INTO links (user_id,link) VALUES ('{$user->id}','{$requestData['link']}')");
            $uniqueCode = $this->getShortenedURLFromID($lastId);
            Database::query("UPDATE links SET short_hash = '{$uniqueCode}' WHERE id=" . $lastId);
            Database::commitTransaction();
        } catch (\Exception $exception) {
            Database::rollback();
            return $this->responseJson(['error' => 'Error in database'], 421);
        }

        return $this->responseJson(['link' => url('redirect', ['uniqueHash' => $uniqueCode]), 'id' => $lastId]);
    }

    /**
     * @param $lastId
     * @return string
     */
    private function getShortenedURLFromID($lastId): string
    {
        $lastId += 10000000;
        return base_convert($lastId, 10, 36);
    }

    /**
     * @param $id
     * @return false|string
     */
    public function delete($id): bool|string
    {
        $user = request()->user;
        $checkRow = Database::select("SELECT id,short_hash FROM links WHERE user_id='{$user->id}' AND id={$id};");
        if (is_null($checkRow)) {
            return $this->responseJson(['error' => 'row not found'], 404);
        }
        $result = Database::query("DELETE FROM `links` WHERE `links`.`id` = " . $id);
        Cache::delete($checkRow['short_hash']);
        return $result
            ? $this->responseJson(['message' => 'row successfuly deleted'])
            : $this->responseJson(['error' => 'database problem'], 421);
    }

    /**
     * @param $id
     * @return false|string
     */
    public function update($id): bool|string
    {
        $user = request()->user;
        $requestData = input()->all();
        $checkRow = Database::select("SELECT id,short_hash FROM links WHERE user_id='{$user->id}' AND id={$id};");
        if (is_null($checkRow)) {
            return $this->responseJson(['error' => 'row not found'], 404);
        }
        if (!filter_var($requestData['link'], FILTER_VALIDATE_URL)) {
            return $this->responseJson(['error' => 'link is not a valid URL'], 421);
        }
        $result = Database::query("UPDATE links SET link = '{$requestData['link']}' WHERE id=" . $checkRow['id']);
        Cache::delete($checkRow['short_hash']);
        return $result
            ? $this->responseJson(['message' => 'row successfuly updated'])
            : $this->responseJson(['error' => 'database problem'], 421);
    }
}