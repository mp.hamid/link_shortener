<?php

namespace App\Base\Facades;

use App\Base\DatabaseConnection;

/**
 * @method static void beginTransaction() beginTransaction() Begin Transaction
 * @method static void commitTransaction() commitTransaction() Commit Transaction
 * @method static void rollback() rollback() Rollback Transaction
 * @method static $this select() select(string $query) Select one row in query
 * @method static $this get() get(string $query) Get query result
 * @method static $this query() query(string $query) Run query in databse
 * @method static string escapeString() escapeString(string $string) Escape string
 */
class Database extends Facade
{
    protected static function getFacadeAccessor()
    {
        return new DatabaseConnection();
    }
}