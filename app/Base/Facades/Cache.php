<?php

namespace App\Base\Facades;


use Predis\Client;

/**
 * @method static bool set() set($key, $value) Set value
 * @method static mixed get() get($key) Get data
 * @method static bool has() has($key) Check key exist
 * @method static bool delete() delete($key) Delete key
 */
class Cache extends Facade
{
    protected static function getFacadeAccessor()
    {
        return new Client([
            'scheme' => 'tcp',
            'host'   => $_ENV['REDIS_HOST'],
            'port'   => $_ENV['REDIS_PORT'],
        ]);
    }
}