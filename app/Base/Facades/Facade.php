<?php

namespace App\Base\Facades;

use RuntimeException;

abstract class Facade
{
    protected static $resolvedInstance;

    protected static function getFacadeAccessor()
    {
        if(method_exists(static::class,'getFacadeAccessor')){
            return static::getFacadeAccessor();
        }
        throw new RuntimeException('Facade does not implement getFacadeAccessor method.');
    }

    protected static function resolveFacadeInstance()
    {
        $name = (new \ReflectionClass(static::class))->getShortName();
        if (isset(static::$resolvedInstance[$name])) {
            return static::$resolvedInstance[$name];
        }

        return static::$resolvedInstance[$name] = self::getFacadeAccessor();
    }

    protected static function getFacadeRoot()
    {
        return self::resolveFacadeInstance();
    }

    public static function __callStatic($method, $args)
    {
        $instance = static::getFacadeRoot();

        if (!$instance) {
            throw new RuntimeException('A facade root has not been set.');
        }

        return $instance->$method(...$args);
    }
}