<?php

namespace App\Base;

class DatabaseConnection
{
    /**
     * @var \mysqli
     */
    private $conn;

    public function __construct()
    {
        $this->conn = new \mysqli($_ENV['DB_HOST'],
            $_ENV['DB_USERNAME'],
            $_ENV['DB_PASSWORD'],
            $_ENV['DB_DATABASE'],
            $_ENV['DB_PORT']);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        return $this;
    }

    public function __destruct()
    {
        $this->conn->close();
    }

    public function beginTransaction()
    {
        $this->conn->begin_transaction();
    }

    public function commitTransaction()
    {
        $this->conn->commit();
    }

    public function rollback()
    {
        $this->conn->rollback();
    }

    public function get(string $query)
    {
        $result = mysqli_query($this->conn, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function select(string $query)
    {
        $result = mysqli_query($this->conn, $query);
        return mysqli_fetch_array($result);
    }

    public function query($query)
    {
        return $this->conn->query($query);
    }

    public function insertWithId($query)
    {
        $this->conn->query($query);
        return mysqli_insert_id($this->conn);
    }

    public function escapeString($string): string
    {
        return mysqli_real_escape_string($this->conn, $string);
    }

    public function getLastId()
    {
        $result = $this->select('SELECT LAST_INSERT_ID()');
        return $result[0];
    }

}
