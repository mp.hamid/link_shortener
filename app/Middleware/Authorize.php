<?php

namespace App\Middleware;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;

class Authorize implements IMiddleware
{

    public function handle(Request $request): void
    {
        $bearerToken = $request->getHeader('http_authorization');
        if (is_null($bearerToken)) {
            $this->Unauthorize();
            return;
        }
        $bearerToken = str_replace('Bearer ', '', $bearerToken);

        $passphrase = $_ENV['PASSPHRASE'];
        $privateKeyFile = storage_path('key-with-passphrase.pem');
        $privateKey = openssl_pkey_get_private(
            file_get_contents($privateKeyFile),
            $passphrase
        );
        $publicKey = openssl_pkey_get_details($privateKey)['key'];

        try {
            $decoded = JWT::decode($bearerToken, new Key($publicKey, 'RS256'));
        } catch (\Exception $exception) {
            $this->Unauthorize();
        }
        if ($decoded->expire_at < time()) {
            $this->Unauthorize();
        }
        unset($decoded->expire_at);
        $request->user = $decoded;
    }

    private function Unauthorize()
    {
        \response()->httpCode(401)->json(['status' => 'unauthorize']);
    }
}